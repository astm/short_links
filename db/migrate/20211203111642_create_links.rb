class CreateLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      # used 'integer' instead of 'references' for allow the unregistered user to add short links
      t.integer :user_id
      t.string :url,       null: false, default: ''
      t.string :prefix,    null: false, default: ''
      t.integer :visits,   null: false, default: 0

      t.timestamps
    end

    # unique fields
    add_index :links, :prefix, unique: true
  end
end
