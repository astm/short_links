# Default Users
unless User.any?
  User.create!(email: 'user@links.com', password: 'useruser', password_confirmation: 'useruser')
  puts 'Users created'
end

# Default Links
unless Link.any?
  # Links without users
  20.times do |i|
    Link.create!(user_id: 0, url: Faker::Internet.url, prefix: SecureRandom.hex(4), visits: i)
  end

  # Links for users
  20.times do |i|
    Link.create!(user_id: User.first.id, url: Faker::Internet.url, prefix: SecureRandom.hex(4), visits: i)
  end

  puts 'Links created'
end
