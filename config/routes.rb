Rails.application.routes.draw do

  # For Home
  get '/', to: 'home#index', as: 'home'

  devise_for :users

  resources :links, only: [:index, :create]
  get '/l/:prefix', to: 'links#show', as: :short_link
end
