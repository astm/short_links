# https://docs.docker.com/samples/rails/
# syntax=docker/dockerfile:1
FROM ruby:2.7.0

# Install nodejs & postgresql
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client

# Create work directory
WORKDIR /myapp

# Copy all the application's files into the /myapp directory.
COPY . /myapp

# Copy docker env file
RUN rm -rf /myapp/.env
RUN cp -r /myapp/.env.docker /myapp/.env

# Run bundle install to install the Ruby dependencies.
RUN gem install bundler -v 2.1.4
RUN bundle lock --add-platform x86-mingw32 x86-mswin32 x64-mingw32 java
RUN bundle install

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/

RUN chmod +x /usr/bin/entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]

EXPOSE 3000

# Configure the main process to run when running the image
CMD ["bash", "rails", "server", "--binding", "0.0.0.0"]
