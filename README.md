[![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop-hq/rubocop)

== README

The Short links application is built using Ruby on Rails v 6.0.3, an MVC web application framework.
This application built to manage the simple short links app.

## Things you may want to cover: ##
- Ruby version: 2.7.0
- Rails version: 6.0.3
- Database: Postgresql

# Installing locally steps #

First you need to make sure that your machine has Ruby & Postgres database then you need to run the below commands

## Clone the code ##
```
git clone https://astm@bitbucket.org/astm/short_links.git
```

## Start Postgres server ##
```
# On Mac
pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start

# On linux
sudo service postgresql start
```

## Database creation ##
```
bundle install
bundle exec rake db:create
bundle exec rake db:migrate
# seed the Database with some test data to insert default administration data.
bundle exec rake db:seed
```

## Test code smell ##
```
rubocop
```

## Start rails server ##
```
rails s
```
## Open on the browser ##
```
http://localhost:3000
```

# Run through docker #
First you need to make sure that your machine has docker then you need to run the below commands

## Create images ##
```
docker-compose build
```

## Create containers ##
```
docker-compose up
```

## Database creation ##
```
docker-compose run web rails db:create
docker-compose run web rails db:migrate
docker-compose run web rake db:seed
```

## Open App ##
```
http://localhost:3000
```

## Stop & delete containers ##
```
docker-compose down
```

# Documentations #

## Database diagram ##
[Diagram](/erd.pdf)
