class LinksController < ApplicationController
  # GET links/index
  def index
    # List all links
    @links = if current_user.present?
               current_user.links.newest_first.page(params[:page])
             else
               Link.newest_first.page(params[:page])
             end
  end

  # GET l/prefix
  def show
    link = Link.find_by(prefix: params[:prefix])
    if link.present?
      # Update visits
      link.update(visits: link.visits + 1)
      redirect_to link.url
    else
      render 'links/error', status: 404
    end
  end

  # POST links/create
  def create
    user = current_user.present? ? current_user.id : 0
    link = Link.new(link_params.merge(user_id: user, prefix: link_params[:url]))

    if link.save
      link.generate_prefix
      redirect_to home_path(link_prefix: link.prefix), notice: 'Link was successfully created.'
    else
      redirect_to home_path, flash: { error: 'Error in creation.' }
    end
  end

  private

  def link_params
    params.require(:link).permit(:user_id, :url, :prefix)
  end
end
