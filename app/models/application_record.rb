class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # Orders
  scope :newest_first, -> { order(created_at: :desc) }
  scope :oldest_first, -> { order(created_at: :asc) }
end
