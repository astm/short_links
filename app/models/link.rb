class Link < ApplicationRecord
  # DB Relations
  belongs_to :user, optional: true

  # Validation
  validates :url, :prefix, presence: true
  validates :prefix, uniqueness: { case_sensitive: false }

  # Create uniq prefix
  def generate_prefix
    self.update(prefix: self.id.to_s + SecureRandom.hex(4))
  end
end
